'use strict';

import Clock from './classes/Clock.js';
import ServerService from './classes/ServerService.js';
import Renderer from './classes/Renderer.js';

new Clock(ServerService, Renderer);