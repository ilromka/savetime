export default class Renderer{

    static get config(){
        return {
            container: 'clockDiv',
            select: 'zoneSelect'
        };
    }

    constructor(Clock){
        this.Clock = Clock;
        let self = Renderer;
        this.container = document.getElementById(self.config.container);
        this.select = document.getElementById(self.config.select);
        this.setZones();
    }

    setZones(){
        this.Clock.config.timezones.forEach((zone, index) => {
            let option = document.createElement('option');
            let text = document.createTextNode(zone.name);
            option.setAttribute('value', index);
            option.appendChild(text);
            this.select.appendChild(option);
        });
    }

    render(data){
        let day = (data.day < 10) ? '0' + data.day : data.day;
        let month = (data.month < 10) ? '0' + data.month : data.month;
        let year = data.year;
        let hours = (data.hours < 10) ? '0' + data.hours : data.hours;
        let minutes = (data.minutes < 10) ? '0' + data.minutes : data.minutes;
	    let seconds = (data.seconds < 10) ? '0' + data.seconds : data.seconds;

        this.container.innerHTML = day + '.' + month + '.' + year + ' ' + hours + ':' + minutes + ':' + seconds;
    }

}