export default class ServerService{

    static get config(){
        return {
            url: '/',
            timeout: 500
        };
    }

    getServerTime(){
        let self = ServerService;
        return new Promise(resolve => {
            let responseTimeout = setTimeout(() => {
                clearTimeout(responseTimeout);
                let date = new Date();
                date.setTime(Date.now());
                resolve({
                    data: {
                        success: true,
                        time: date.getTime()
                    }
                });
            }, self.config.timeout);
        })
    }

}