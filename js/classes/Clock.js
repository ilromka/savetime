export default class Clock{

    static get config(){
        return {
            timezones: [
                {
                    name: 'Москва',
                    zoneName: 'Europe/Moscow'
                },
                {
                    name: 'Нью-Йорк',
                    zoneName: 'America/New_York'
                }
            ]
        };
    }

    constructor(ServerService, Renderer){
        this.ServerService = new ServerService();
        this.Renderer = new Renderer(Clock);
        this.zone = 0;
        this.time = null;
        this.getServerTime();
        this.setEvents();
        this.timer();
    }

    timer(){
        setInterval(() => {
            this.time += 1000;
	        this.renderDate(this.time);
        }, 1000);
    }

    setEvents(){
        this.select = document.getElementById('zoneSelect');
        this.select.onchange = () => {
            this.setZone(this.select.value);
        };

        this.button = document.getElementById('requestButton');
        this.button.onclick = () => {
            this.getServerTime();
        };
    }

    getServerTime(){
        this.ServerService.getServerTime().then(
            response => {
                if(response.data.success){
                    this.time = response.data.time;
                    this.renderDate(this.time);
                }
            },
            () => {
                console.log('error');
            }
        );
    }

    renderDate(time){
        let self = Clock;
        let date = new Date();
        date.setTime(time);
        let dateUTC = new Date(date.toLocaleString('en-US', {timeZone: self.config.timezones[this.zone].zoneName}));

        let data = {
            day: dateUTC.getDate(),
            month: dateUTC.getMonth(),
            year: dateUTC.getFullYear(),
            hours: dateUTC.getHours(),
            minutes: dateUTC.getMinutes(),
            seconds: dateUTC.getSeconds()
        };

        this.Renderer.render(data);
    }

    setZone(zone){
        this.zone = zone;
        this.renderDate(this.time);
    }

}